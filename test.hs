-- Example module
--
-- This is a simple example of a module definition

module Simple
where

snoc :: a -> [a] -> [a]
x `snoc` xs = xs ++ [x]

reverse :: [a] -> [a]
reverse []     = []
reverse (x:xs) = x `snoc` Simple.reverse xs

-- Main method

main :: IO ()
main = do {
    print (Simple.reverse "hello");
    return ()
    }
